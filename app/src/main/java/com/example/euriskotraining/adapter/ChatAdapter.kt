package com.example.euriskotraining.adapter


import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.euriskotraining.databinding.*
import com.example.euriskotraining.data.entity.ChatEntity
import com.example.euriskotraining.utilities.*
import com.example.euriskotraining.utilities.GlobalVariables.Companion.MESSAGE_TYPE_TEXT
import com.example.euriskotraining.utilities.GlobalVariables.Companion.VIEW_TYPE_MESSAGE_RIGHT
import java.io.File
import kotlin.collections.ArrayList

class RightChatHolder(binding: ItemChatRightBinding) : RecyclerView.ViewHolder(binding.root) {
    val message: TextView = binding.tvMessage
    val time: TextView = binding.tvTime
    val date: TextView = binding.tvDate
}

class LeftChatHolder(binding: ItemChatLeftBinding) : RecyclerView.ViewHolder(binding.root) {
    val message: TextView = binding.tvMessage
    val time: TextView = binding.tvTime
    val date: TextView = binding.tvDate
}

class ImageRightHolder(binding: ItemImageRightBinding) : RecyclerView.ViewHolder(binding.root) {
    val image: ImageView = binding.ivImage
    val time: TextView = binding.tvTime
    val date: TextView = binding.tvDate
}

class ImageLeftHolder(binding: ItemImageLeftBinding) : RecyclerView.ViewHolder(binding.root) {
    val image: ImageView = binding.ivImage
    val time: TextView = binding.tvTime
    val date: TextView = binding.tvDate
}
class AudioHolder(binding: ItemAudioRecordingRightBinding) : RecyclerView.ViewHolder(binding.root) {
    val play: ImageView = binding.ivPlay
    val stop: ImageView = binding.ivPause
    val processTime: TextView = binding.tvTimeRecorder
    val time: TextView = binding.tvTime
    val date: TextView = binding.tvDate
    val bar: SeekBar = binding.sbSeekBar
}
class DocumentHolder(binding: ItemDocumentRightBinding) : RecyclerView.ViewHolder(binding.root) {
    val document: TextView = binding.tvDocument
    val time: TextView = binding.tvTime
    val date: TextView = binding.tvDate
}

const val RIGHT_TEXT = 0
const val LEFT_TEXT = 1
const val RIGHT_IMAGE = 2
const val LEFT_IMAGE = 3
const val AUDIO_RIGHT = 4
const val DOCUMENT_RIGHT = 5

class ChatAdapter(private var messages: ArrayList<ChatEntity>, private val activity: Activity) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        when (viewType) {
            RIGHT_TEXT -> {
                val binding =
                    ItemChatRightBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return RightChatHolder(binding)
            }
            LEFT_TEXT -> {
                val binding =
                    ItemChatLeftBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return LeftChatHolder(binding)
            }
            RIGHT_IMAGE -> {
                val binding =
                    ItemImageRightBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                return ImageRightHolder(binding)
            }
            LEFT_IMAGE -> {
                val binding =
                    ItemImageLeftBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return ImageLeftHolder(binding)
            }
            AUDIO_RIGHT -> {
                val binding =
                    ItemAudioRecordingRightBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return AudioHolder(binding)
            }
            else -> {
                val binding =
                    ItemDocumentRightBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return DocumentHolder(binding)
            }
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val currentChat = messages[position]
        val curDay = DateUtils.getDayFromStr(currentChat.date)
        val prevDay = DateUtils.getDayFromStr(getPreviousMessage(position).date)

        if (position == 0 || curDay > prevDay) {
            val today = DateUtils.getToday()
            val yesterday = DateUtils.getYesterday()
            when (curDay) {
                today -> {
                    displayDataInViewHolder(
                        holder,
                        position,
                        GlobalVariables.TODAY,
                        DateUtils.getTimeFormattedFromStr(currentChat.date),
                        currentChat
                    )
                }
                yesterday -> {
                    displayDataInViewHolder(
                        holder,
                        position,
                        GlobalVariables.YESTERDAY,
                        DateUtils.getTimeFormattedFromStr(currentChat.date),
                        currentChat
                    )
                }
                else -> {
                    displayDataInViewHolder(
                        holder,
                        position,
                        DateUtils.getDayFormattedFromStr(currentChat.date),
                        DateUtils.getTimeFormattedFromStr(currentChat.date),
                        currentChat
                    )
                }
            }

        } else {
            displayDataInViewHolder(
                holder,
                position,
                "",
                DateUtils.getTimeFormattedFromStr(currentChat.date),
                currentChat
            )
        }

    }


    override fun getItemCount(): Int {
        return messages.size
    }

    fun loadNewData(newContents: List<ChatEntity>) {
        val diffCallback = MessagesCallBack(messages, newContents)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        messages.clear()
        messages.addAll(newContents)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemViewType(position: Int): Int {
        val currentMessage = messages[position]
        if (currentMessage.userType == VIEW_TYPE_MESSAGE_RIGHT) {

            return if (currentMessage.messageType == MESSAGE_TYPE_TEXT) {
                RIGHT_TEXT
            } else if(currentMessage.messageType == GlobalVariables.MESSAGE_TYPE_PHOTO) {
                RIGHT_IMAGE
            } else if(currentMessage.messageType == GlobalVariables.MESSAGE_TYPE_AUDIO) {
                AUDIO_RIGHT
            } else {
                DOCUMENT_RIGHT
            }

        } else {

            return if (currentMessage.messageType == MESSAGE_TYPE_TEXT) {
                LEFT_TEXT
            } else {
                LEFT_IMAGE
            }
        }
    }

    private fun getPreviousMessage(position: Int): ChatEntity {
        if (position == 0) {
            return messages[0]
        }
        return messages[position - 1]
    }

    private fun displayDataInViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        date: String,
        time: String,
        currentChat: ChatEntity
    ) {
        val viewType = getItemViewType(position)
        if (viewType == RIGHT_TEXT) {
            val binding = holder as RightChatHolder
            binding.message.text = currentChat.text
            binding.date.text = date
            binding.time.text = time

        } else if (viewType == LEFT_TEXT) {
            val binding = holder as LeftChatHolder
            binding.message.text = currentChat.text
            binding.date.text = date
            binding.time.text = time
        } else if (viewType == RIGHT_IMAGE) {
            val binding = holder as ImageRightHolder
            binding.image.setImageBitmap(
                BitmapUtils.convertFilePathToBitmap(
                    currentChat.file,
                    activity
                )
            )
            binding.date.text = date
            binding.time.text = time

        } else if(viewType == LEFT_IMAGE) {
            val binding = holder as ImageLeftHolder
            binding.image.setImageBitmap(
                BitmapUtils.convertFilePathToBitmap(
                    currentChat.file,
                    activity
                )
            )
            binding.date.text = date
            binding.time.text = time
        } else if(viewType == AUDIO_RIGHT) {
            val binding = holder as AudioHolder
            VoicePlayer.getInstance(activity)?.init(currentChat.file, binding.play,binding.stop, binding.bar, binding.processTime)
            binding.date.text = date
            binding.time.text = time

        } else {
            val binding = holder as DocumentHolder
            val file = File(currentChat.file)
            binding.date.text = date
            binding.time.text = time
            binding.document.text = currentChat.file
            binding.document.setOnClickListener {

            }
        }
    }
}