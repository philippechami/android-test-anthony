package com.example.euriskotraining.adapter


import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.euriskotraining.presentations.ui.activities.MainActivity

class CarouselCardAdapter(private val frs: ArrayList<Fragment>, private val fragActivity: FragmentActivity): FragmentStateAdapter(fragActivity) {
    override fun getItemCount(): Int {
        return frs.size
    }

    override fun createFragment(position: Int): Fragment {
        return frs[position]
    }
}