package com.example.euriskotraining.presentations.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TimerFragmentViewModel: ViewModel() {

    private val timeUpdated = MutableLiveData<String>()
    fun getTimeUpdated() : LiveData<String> {
        return timeUpdated
    }

    fun updateTime(seconds : Long) {
        val hrs = seconds / 3600
        val mins = (seconds % 3600) / 60
        val sec = seconds % 60
        val time = String.format("%02d,%02d,%02d", hrs, mins, sec)
        timeUpdated.postValue(time)
    }



}