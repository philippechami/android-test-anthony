package com.example.euriskotraining.presentations.ui.activities


import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.viewModels
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.euriskotraining.BaseActivity
import com.example.euriskotraining.R
import com.example.euriskotraining.databinding.ActivityMainBinding
import com.example.euriskotraining.data.entity.TimerModel
import com.example.euriskotraining.utilities.GlobalVariables
import com.example.euriskotraining.presentations.viewmodels.MainActivityViewModel
import com.example.euriskotraining.utilities.LanguageConfiguration
import com.example.euriskotraining.utilities.LanguageConfiguration.Companion.changeLanguage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import java.lang.Runnable

@AndroidEntryPoint
class MainActivity : BaseActivity() {
    private lateinit var binding: ActivityMainBinding
    private val mainActivityViewModel: MainActivityViewModel by viewModels()
    private lateinit var mainHandler: Handler


    private val updateTextTask = object : Runnable {
        override fun run() {
            GlobalVariables.secondsFromBegining++
            mainHandler.postDelayed(this, 1000)
        }
    }



    @DelicateCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bottomNavigation = binding.bnvNavigation
        val navController = findNavController(R.id.fragment_nav)

        bottomNavigation.setupWithNavController(navController)

        mainActivityViewModel.getLastTime().observe(this, {
            GlobalVariables.secondsFromBegining = it.timer
            GlobalVariables.databaseCreated = true
        })
        mainActivityViewModel.getTimer(1)
        mainHandler = Handler(Looper.getMainLooper())

    }

    override fun attachBaseContext(newBase: Context?) {
        val sharePref = newBase?.getSharedPreferences(GlobalVariables.PREF_NAME, MODE_PRIVATE)
        val langCode = sharePref?.getString(GlobalVariables.LANG_KEY,GlobalVariables.ENGLISH)
        var contextWrapper = newBase
        langCode?.let{
            contextWrapper = LanguageConfiguration(newBase).changeLanguage(langCode)
        }
        super.attachBaseContext(contextWrapper)
    }

    override fun onResume() {
        mainHandler.post(updateTextTask)
        super.onResume()
    }

    override fun onPause() {
        mainHandler.removeCallbacks(updateTextTask)
        super.onPause()
    }
    @DelicateCoroutinesApi
    override fun onStop(){
            if(GlobalVariables.databaseCreated) {
                mainActivityViewModel.updateTimer(TimerModel(1,GlobalVariables.secondsFromBegining))
            } else {
                mainActivityViewModel.insertTime(TimerModel(1, GlobalVariables.secondsFromBegining))
            }
        super.onStop()
    }
}