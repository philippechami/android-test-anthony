package com.example.euriskotraining.presentations.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.euriskotraining.databinding.FragmentCardRotationBinding

private const val ANGLE = "angle"

class CardRotationFragment : Fragment() {

    private lateinit var binding: FragmentCardRotationBinding
    private var angle: Float = 0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            angle = it.getFloat(ANGLE)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCardRotationBinding.inflate(inflater, container, false)
        return binding.root
    }

    companion object {
        fun newInstance(angle: Float) =
            CardRotationFragment().apply {
                arguments = Bundle().apply {
                    putFloat(ANGLE, angle)
                }
            }
    }
}