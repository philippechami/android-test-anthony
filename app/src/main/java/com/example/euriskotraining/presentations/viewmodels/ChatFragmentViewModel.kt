package com.example.euriskotraining.presentations.viewmodels

import android.app.Activity
import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.*
import com.example.euriskotraining.data.repositories.DatabaseRepo
import com.example.euriskotraining.data.entity.ChatEntity
import com.example.euriskotraining.utilities.BitmapUtils
import com.example.euriskotraining.utilities.DateUtils
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class ChatFragmentViewModel @Inject constructor(private val repo: DatabaseRepo) : ViewModel() {
    private val chats  = MutableLiveData<List<ChatEntity>>()
    fun getMessages() : LiveData<List<ChatEntity>> {
        return chats
    }
    private val imageUri  = MutableLiveData<Uri>()
    fun getImageUri() : LiveData<Uri> {
        return imageUri
    }

    fun sendMessage(messageToSend: String, userType : Int, messageType: Int, imageUri: String = "") {
        viewModelScope.launch {
            val chatToSave = ChatEntity()
            chatToSave.userType = userType
            chatToSave.messageType = messageType
            chatToSave.date = DateUtils.getDateFormatted(Date())
            chatToSave.text = messageToSend
            chatToSave.file = imageUri
            repo.insert(chatToSave)

            val newChats = repo.getMessages()
            newChats?.let {
                chats.postValue(it)
            }

        }
    }

    fun getChats()  {
        viewModelScope.launch {
            chats.postValue(repo.getMessages().orEmpty())
        }
    }

    fun saveImageToExternalStorage(name: String, bmp: Bitmap?, activity:Activity){
        viewModelScope.launch {
            val uri: Uri? = BitmapUtils.saveBitmapToExternalStorage(name, bmp, activity)
            uri?.let {
                imageUri.postValue(it)
            }
        }
    }
}