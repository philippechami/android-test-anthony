package com.example.euriskotraining.presentations.ui.fragments

import android.Manifest
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Build.VERSION.SDK_INT
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import com.example.euriskotraining.BaseFragment
import com.example.euriskotraining.adapter.ChatAdapter
import com.example.euriskotraining.databinding.FragmentChatBinding
import com.example.euriskotraining.dialogs.ImagesDestinationDialog
import com.example.euriskotraining.interfaces.OnAttachmentsClicked
import com.example.euriskotraining.presentations.viewmodels.ChatFragmentViewModel
import dagger.hilt.android.AndroidEntryPoint
import android.media.MediaRecorder
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import com.example.euriskotraining.interfaces.OnRecordButtonClicked
import com.example.euriskotraining.utilities.*
import java.io.File
import java.io.IOException
import java.lang.Exception


@AndroidEntryPoint
class ChatFragment : BaseFragment(), OnAttachmentsClicked {

    private lateinit var binding: FragmentChatBinding
    private lateinit var botMessages: List<String>
    private lateinit var chatAdapter: ChatAdapter
    private val dialog = ImagesDestinationDialog(this)
    private lateinit var mediaRecorder: MediaRecorder
    private lateinit var audioPath: String
    private val chatFragmentViewModel: ChatFragmentViewModel by viewModels()
    private lateinit var recordView: RecordView
    private lateinit var recordButton: RecordButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        botMessages = listOf(
            "message1",
            "message2",
            "message3",
            "message4",
            "message5",
            "message6",
            "message7",
            "message8",
            "message9",
            "message10"

        )

        chatFragmentViewModel.getMessages().observe(this, {
            chatAdapter.loadNewData(it)
        })
        chatFragmentViewModel.getImageUri().observe(this, {
            chatFragmentViewModel.sendMessage(
                "",
                GlobalVariables.VIEW_TYPE_MESSAGE_RIGHT,
                GlobalVariables.MESSAGE_TYPE_PHOTO,
                it.toString()
            )
            sendBotMessage()
        })

        chatAdapter = ChatAdapter(ArrayList(), requireActivity())
        chatFragmentViewModel
        chatFragmentViewModel.getChats()


    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentChatBinding.inflate(inflater, container, false)
        binding.ibSend.setOnClickListener {
            val messageToSend = binding.etSend.text.toString()
            if (messageToSend.isNotEmpty()) {
                chatFragmentViewModel.sendMessage(
                    messageToSend,
                    GlobalVariables.VIEW_TYPE_MESSAGE_RIGHT,
                    GlobalVariables.MESSAGE_TYPE_TEXT
                )
                val randomIndex = (0..9).random()
                chatFragmentViewModel.sendMessage(
                    botMessages[randomIndex],
                    GlobalVariables.VIEW_TYPE_MESSAGE_LEFT,
                    GlobalVariables.MESSAGE_TYPE_TEXT
                )
                binding.etSend.setText("")
            }
        }
        binding.rvMessages.adapter = chatAdapter

        binding.ibAttachments.setOnClickListener {
            openDialog()
        }
        binding.etSend.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.toString().isNullOrBlank()) {

                    binding.ibSend.visibility = View.GONE
                    binding.ibRecord.visibility = View.VISIBLE

                } else {
                    binding.ibSend.visibility = View.VISIBLE
                    binding.ibRecord.visibility = View.GONE

                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
        initRecordView()
        return binding.root
    }

    override fun onGalleryClicked() {

        if (PermissionsUtils.isReadStoragePermissionGranted(requireContext())) {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.type = "image/*"
            intent.flags = Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION
            intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            resultImagePickedFromGalleryLauncher.launch(intent)
        } else {
            requestReadPermission()
        }
        dialog.dismiss()
    }


    override fun onCameraClicked() {
        context?.let {
            if (PermissionsUtils.isCamPermissionGranted(it)) {
                if (PermissionsUtils.isWriteStoragePermissionGranted(it)) {
                    val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    resultImagePickedFromCamLauncher.launch(takePictureIntent)
                } else {
                    requestWritePermission()
                }
            } else {
                requestCameraPermission()
            }
            dialog.dismiss()
        }

    }

    override fun onDocumentsClicked() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.type = "application/pdf"
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        resultDocumentChoosenLauncher.launch(intent)
        dialog.dismiss()
    }


    private fun sendBotMessage() {
        val randomIndex = (0..9).random()
        chatFragmentViewModel.sendMessage(
            botMessages[randomIndex],
            GlobalVariables.VIEW_TYPE_MESSAGE_LEFT,
            GlobalVariables.MESSAGE_TYPE_TEXT
        )
    }

    private fun openDialog() {
        activity?.let {
            dialog.show(it.supportFragmentManager, "imageDestinationDialog")
        }
    }


    private fun requestCameraPermission() {
        requestCameraPermissionLauncher.launch(Manifest.permission.CAMERA)
    }

    private fun requestReadPermission() {
        val isReadPermissionGranted =
            PermissionsUtils.isReadStoragePermissionGranted(requireContext())
        if (!isReadPermissionGranted) {
            requestReadPermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
    }

    private fun requestWritePermission() {
        val isWritePermission = PermissionsUtils.isWriteStoragePermissionGranted(requireContext())
        val minSDK = SDK_INT >= Build.VERSION_CODES.Q
        val isWritePermissionGranted = isWritePermission || minSDK
        if (!isWritePermissionGranted) {
            requestWritePermissionLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
    }
    private fun requestAudioRecordPermissions() {
        context?.let { context ->

            if (!PermissionsUtils.isReadStoragePermissionGranted(context)) {
                requestPermissionForAudioRecord.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
            } else if (!PermissionsUtils.isWriteStoragePermissionGranted(context)) {
                requestPermissionForAudioRecord.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            } else if (!PermissionsUtils.isRecordPermissionGranted(context)) {
                requestPermissionForAudioRecord.launch(Manifest.permission.RECORD_AUDIO)
            } else {
                recordButton.isListenForRecording = true
            }
        }
    }

    private fun initRecordView() {



        recordView = RecordView(requireContext())
        recordView.init(binding.rlRecordViewParent, binding.ivGlowingMic, binding.cCounterAudio)
        recordButton = RecordButton(requireContext())

        recordButton.init(recordView, binding.ibRecord, object : OnRecordButtonClicked{
            override fun onClick() {
                requestAudioRecordPermissions()
            }

        })
        if(PermissionsUtils.isReadStoragePermissionGranted(requireContext())
            && PermissionsUtils.isWriteStoragePermissionGranted(requireContext())
            && PermissionsUtils.isRecordPermissionGranted(requireContext())) {
            recordButton.isListenForRecording = true
        }

        recordView.setOnRecordListener(object :
            com.example.euriskotraining.interfaces.OnRecordListener {
            override fun onStart() {
                setUpRecording()
                try {
                    mediaRecorder.prepare()
                    mediaRecorder.start()
                } catch (e: IOException) {
                    LocalFunctions.printException(e)
                }
                binding.rlMessageLayout.visibility = View.GONE
            }

            override fun onCancel() {

            }

            override fun onFinish() {
                try {
                    mediaRecorder.stop()
                    mediaRecorder.release()
                } catch (e: Exception) {
                    LocalFunctions.printException(e)
                }
                binding.rlMessageLayout.visibility = View.VISIBLE

                chatFragmentViewModel.sendMessage(
                    "",
                    GlobalVariables.VIEW_TYPE_MESSAGE_RIGHT,
                    GlobalVariables.MESSAGE_TYPE_AUDIO,
                    audioPath
                )
            }

            override fun onLessThanSecond() {

            }

        })


    }

    private fun setUpRecording() {
        context?.let {
            mediaRecorder = if (SDK_INT >= Build.VERSION_CODES.S) {
                MediaRecorder(it)

            } else {
                MediaRecorder()
            }
            with(mediaRecorder) {
                setAudioSource(MediaRecorder.AudioSource.MIC)
                setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
                setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
            }
            val file = File(it.getExternalFilesDir(null)?.absolutePath, "Media/Recording")
            if (!file.exists()) {
                file.mkdirs()
            }
            audioPath =
                file.absolutePath + File.separator + System.currentTimeMillis().toString() + ".3gp"
            Log.d("AUDIO", audioPath)
            mediaRecorder.setOutputFile(audioPath)
        }

    }



    private val requestPermissionForAudioRecord =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                requestAudioRecordPermissions()
            }

        }
    private val requestWritePermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                onCameraClicked()
            }
        }
    private val requestCameraPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                onCameraClicked()
            }
        }
    private val requestReadPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                onGalleryClicked()
            }
        }

    private var resultImagePickedFromCamLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { activityResult ->

            val data = activityResult.data?.extras?.get("data")
            data?.let {
                val bitmapToStore: Bitmap = data as Bitmap
                activity?.let { it1 ->
                    chatFragmentViewModel.saveImageToExternalStorage(
                        "image",
                        bitmapToStore,
                        it1
                    )
                }
            }
        }

    private var resultImagePickedFromGalleryLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            it?.data?.data?.let { uri ->
                context?.contentResolver?.takePersistableUriPermission(
                    uri,
                    Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                )

                chatFragmentViewModel.sendMessage(
                    "",
                    GlobalVariables.VIEW_TYPE_MESSAGE_RIGHT,
                    GlobalVariables.MESSAGE_TYPE_PHOTO,
                    uri.toString()
                )
                sendBotMessage()
            }

        }

    private var resultDocumentChoosenLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            it?.data?.data?.let { uri ->
                val uriPath = uri.path
                if (uriPath != null) {
                    Log.d("FILEEE", uriPath)
                    val file = File(uriPath)
                    file.mkdirs()

                    chatFragmentViewModel.sendMessage(
                        "",
                        GlobalVariables.VIEW_TYPE_MESSAGE_RIGHT,
                        GlobalVariables.MESSAGE_TYPE_DOCUMENT,
                        uriPath
                    )
                }

            }

        }
}