package com.example.euriskotraining

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import com.example.euriskotraining.adapter.CarouselCardAdapter
import com.example.euriskotraining.databinding.FragmentCarouselCardBinding
import com.example.euriskotraining.presentations.ui.fragments.CardRotationFragment
import com.example.euriskotraining.utilities.CarouselTransformer


class CarouselCardFragment : Fragment() {

    private lateinit var binding: FragmentCarouselCardBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCarouselCardBinding.inflate(inflater, container, false)
        val frs: ArrayList<Fragment> = arrayListOf(
            CardRotationFragment.newInstance(0f),
            CardRotationFragment.newInstance(0f),
            CardRotationFragment.newInstance(0f),
        )
        val adapter = CarouselCardAdapter(frs, requireActivity())
        binding.vp2CardCarousel.clipToPadding = false
        binding.vp2CardCarousel.clipChildren = false
        binding.vp2CardCarousel.offscreenPageLimit = 3

        binding.vp2CardCarousel.adapter = adapter
        binding.vp2CardCarousel.setPageTransformer(CarouselTransformer())
        return binding.root
    }
}

