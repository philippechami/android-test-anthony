package com.example.euriskotraining.presentations.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.example.euriskotraining.BaseFragment
import com.example.euriskotraining.databinding.FragmentTimerBinding
import com.example.euriskotraining.presentations.viewmodels.TimerFragmentViewModel
import com.example.euriskotraining.utilities.GlobalVariables


class TimerFragment : BaseFragment() {

    private lateinit var binding: FragmentTimerBinding
    private val timerFragmentViewModel: TimerFragmentViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        timerFragmentViewModel.getTimeUpdated().observe(this, {
            binding.tvTimer.text = it
        })
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        binding = FragmentTimerBinding.inflate(inflater, container, false)

        val mainHandler = Handler(Looper.getMainLooper())
        mainHandler.post(object : Runnable {
            override fun run() {
                timerFragmentViewModel.updateTime(GlobalVariables.secondsFromBegining)
                mainHandler.postDelayed(this, 1000)
            }
        })


        return binding.root
    }



}