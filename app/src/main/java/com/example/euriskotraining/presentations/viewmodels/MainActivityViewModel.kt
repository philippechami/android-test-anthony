package com.example.euriskotraining.presentations.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.euriskotraining.data.repositories.DatabaseRepo
import com.example.euriskotraining.data.entity.TimerModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class MainActivityViewModel @Inject constructor(private val repo: DatabaseRepo) : ViewModel() {

    private val lastTime = MutableLiveData<TimerModel>()

    fun getLastTime(): LiveData<TimerModel>{
        return lastTime
    }

    fun getTimer(id: Long){
        viewModelScope.launch {
            val timer = repo.getTimer(id)
            timer?.let {
                lastTime.postValue(it)
            }
        }

    }
    fun updateTimer(time: TimerModel){
        viewModelScope.launch {
            repo.update(time)
        }

    }

     fun insertTime(time : TimerModel){
         viewModelScope.launch {
             repo.insert(time)
         }

    }



}