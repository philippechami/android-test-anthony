package com.example.euriskotraining.presentations.ui.fragments

import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.euriskotraining.databinding.FragmentSettingsBinding
import com.example.euriskotraining.presentations.ui.activities.MainActivity
import com.example.euriskotraining.utilities.GlobalVariables


class SettingsFragment : Fragment() {

    private lateinit var binding: FragmentSettingsBinding
    private lateinit var sharePref: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharePref = requireActivity().getSharedPreferences(GlobalVariables.PREF_NAME, MODE_PRIVATE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentSettingsBinding.inflate(layoutInflater, container, false)
        val langCode = sharePref.getString(GlobalVariables.LANG_KEY, GlobalVariables.ENGLISH)
        if(!binding.scLanguage.isChecked && langCode == GlobalVariables.ARABIC){
            binding.scLanguage.toggle()
        }
        if(binding.scLanguage.isChecked && langCode == GlobalVariables.ENGLISH) {
            binding.scLanguage.toggle()
        }

        binding.scLanguage.setOnClickListener {
            if(binding.scLanguage.isChecked){
                save(GlobalVariables.LANG_KEY, GlobalVariables.ARABIC)
            } else {
                save(GlobalVariables.LANG_KEY, GlobalVariables.ENGLISH)
            }
        }
        return binding.root
    }


    private fun save(key: String, value: String) {
        val sharePref = requireActivity().getSharedPreferences(GlobalVariables.PREF_NAME, MODE_PRIVATE)
        sharePref.edit().apply {
            this.putString(key, value)
            this.commit()
        }
        startActivity(Intent(requireActivity(), MainActivity::class.java))
    }

}