package com.example.euriskotraining.utilities

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import java.io.IOException
import kotlin.math.roundToInt

class BitmapUtils {
    companion object{
        fun convertFilePathToBitmap(imagePath: String, activity: Activity): Bitmap {

            val imgUri = Uri.parse(imagePath)
            val bitmapImage: Bitmap

             if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                 val source =ImageDecoder.createSource(activity.contentResolver, imgUri)
                 bitmapImage = ImageDecoder.decodeBitmap(source)

            } else {
                 bitmapImage = MediaStore.Images.Media.getBitmap(activity.contentResolver, imgUri);
            }
            val width = pxFromDp(GlobalVariables.IMAGE_WIDTH_DP,activity)
            val height = pxFromDp(GlobalVariables.IMAGE_HEIGHT_DP,activity)
            return Bitmap.createScaledBitmap(bitmapImage, width, height, true)
        }

        fun saveBitmapToExternalStorage(name: String, bmp: Bitmap?, activity:Activity): Uri?{
            var imageUri: Uri? = null
                val collectionImage : Uri = if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                    MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
                } else {
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                }
                val contentValues = ContentValues().apply {
                    put(MediaStore.Images.Media.DISPLAY_NAME,"$name.jpg")
                    put(MediaStore.Images.Media.MIME_TYPE,"image/jpeg")
                    if(bmp!=null){
                        put(MediaStore.Images.Media.WIDTH,bmp.width)
                        put(MediaStore.Images.Media.HEIGHT,bmp.height)
                    }
                }

                try{
                    activity.contentResolver?.insert(collectionImage, contentValues)?.also {uri->
                        activity.contentResolver?.openOutputStream(uri).use {outputStream ->
                            if(bmp != null) {
                                if(!bmp.compress(Bitmap.CompressFormat.JPEG, 95, outputStream)){
                                    throw IOException("Failed to save Bitmap")
                                }
                            }
                            imageUri = uri
                        }

                    }?: throw IOException("Failed to create Media Store entry")

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            return imageUri
        }
        private fun pxFromDp(dp: Int, context: Context): Int{
            return dp * context.resources.displayMetrics.density.roundToInt()
        }
    }
}