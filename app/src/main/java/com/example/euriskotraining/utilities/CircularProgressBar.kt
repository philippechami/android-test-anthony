package com.example.euriskotraining.utilities

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.core.content.ContextCompat
import com.example.euriskotraining.R
import kotlin.math.cos
import kotlin.math.sin

class CircularProgressBar : View {
    private var mText: String? = ""
    private var mUnit: String? = ""
    private var mPoints = MIN
    private var mMin = MIN
    private var mMax = MAX
    private var step = 10
    private var mIndicatorIcon: Drawable? = null
    private var mProgressWidth = 12
    private var mArcWidth = 12
    private var isClockwise = true
    private var mEnabled = true
    private var mUpdateTimes = 0
    private var mPreviousProgress = -1f
    private var mCurrentProgress = 0f
    private var isMax = false
    private var isMin = false
    private var mArcRadius = 0
    private val mArcRect = RectF()
    private var mArcPaint: Paint = Paint()
    private var mProgressSweep = 0f
    private var mProgressPaint: Paint = Paint()
    private var mRoundedProgress = false
    private var mValueSize = 72f
    private var mValuePaint: Paint = Paint()
    private val mValueRect = Rect()
    private var mTextSize = mValueSize / 2
    private var mTextPaint: Paint = Paint()
    private val mTextRect = Rect()
    private var mTranslateX = 0
    private var mTranslateY = 0

    // the (x, y) coordinator of indicator icon
    private var mIndicatorIconX = 0
    private var mIndicatorIconY = 0
    private var mTouchAngle = 0.0
    private var mOnPointsChangeListener: OnPointsChangeListener? = null

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        val density = resources.displayMetrics.density

        // Defaults, may need to link this into theme settings
        var arcColor = ContextCompat.getColor(context, R.color.design_default_color_secondary)
        var progressColor = ContextCompat.getColor(context, R.color.design_default_color_primary)
        var textColor = ContextCompat.getColor(context, R.color.design_default_color_secondary)
        var valueColor = ContextCompat.getColor(context, R.color.design_default_color_primary)
        mProgressWidth = (mProgressWidth * density).toInt()
        mArcWidth = (mArcWidth * density).toInt()
        mTextSize = (mTextSize * density)
        if (attrs != null) {
            // Attribute initialization
            val a = context.obtainStyledAttributes(attrs, R.styleable.CircularProgressBar, 0, 0)
            val indicatorIcon = a.getDrawable(R.styleable.CircularProgressBar_indicatorIcon)
            if (indicatorIcon != null) {
                mIndicatorIcon = indicatorIcon
            }
            val indicatorIconHalfWidth = mIndicatorIcon?.intrinsicWidth?.div(2)
            val indicatorIconHalfHeight = mIndicatorIcon?.intrinsicHeight?.div(2)
            if(indicatorIconHalfHeight != null && indicatorIconHalfWidth != null) {
                mIndicatorIcon?.setBounds(
                    -indicatorIconHalfWidth, -indicatorIconHalfHeight, indicatorIconHalfWidth,
                    indicatorIconHalfHeight
                )
            }

            mPoints = a.getInteger(R.styleable.CircularProgressBar_points, mPoints)
            mText = a.getString(R.styleable.CircularProgressBar_text)
            mUnit = a.getString(R.styleable.CircularProgressBar_unit)
            mMin = a.getInteger(R.styleable.CircularProgressBar_min, mMin)
            mMax = a.getInteger(R.styleable.CircularProgressBar_max, mMax)
            step = a.getInteger(R.styleable.CircularProgressBar_step, step)

            mProgressWidth = a.getDimension(R.styleable.CircularProgressBar_progressWidth, mProgressWidth.toFloat()).toInt()
            progressColor = a.getColor(R.styleable.CircularProgressBar_progressColor, progressColor)
            mArcWidth = a.getDimension(R.styleable.CircularProgressBar_arcWidth, mArcWidth.toFloat()).toInt()
            arcColor = a.getColor(R.styleable.CircularProgressBar_arcColor, arcColor)
            mTextSize = a.getDimension(R.styleable.CircularProgressBar_textSize, mTextSize)
            mValueSize = a.getDimension(R.styleable.CircularProgressBar_valueSize, mValueSize)
            textColor = a.getColor(R.styleable.CircularProgressBar_textColor, textColor)
            valueColor = a.getColor(R.styleable.CircularProgressBar_valueColor, valueColor)
            isClockwise = a.getBoolean(R.styleable.CircularProgressBar_clockwise, isClockwise)
            mEnabled = a.getBoolean(R.styleable.CircularProgressBar_enabled, mEnabled)
            mRoundedProgress = a.getBoolean(R.styleable.CircularProgressBar_roundedProgress, mRoundedProgress)
            a.recycle()
        }

        // range check
        mPoints = if (mPoints > mMax) mMax else mPoints
        mPoints = if (mPoints < mMin) mMin else mPoints
        mProgressSweep = mPoints.toFloat() / valuePerDegree()

        mArcPaint.color = arcColor
        mArcPaint.isAntiAlias = true
        mArcPaint.style = Paint.Style.STROKE
        mArcPaint.strokeWidth = mArcWidth.toFloat()

        mProgressPaint.color = progressColor
        mProgressPaint.isAntiAlias = true
        mProgressPaint.style = Paint.Style.STROKE
        mProgressPaint.strokeWidth = mProgressWidth.toFloat()
        if(mRoundedProgress) {
            mProgressPaint.strokeCap = Paint.Cap.ROUND
        }

        mTextPaint.color = textColor
        mTextPaint.isAntiAlias = true
        mTextPaint.style = Paint.Style.FILL
        mTextPaint.textSize = mTextSize

        mValuePaint.color = valueColor
        mValuePaint.isAntiAlias = true
        mValuePaint.style = Paint.Style.FILL
        mValuePaint.textSize = mValueSize
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = getDefaultSize(suggestedMinimumWidth, widthMeasureSpec)
        val height = getDefaultSize(suggestedMinimumHeight, heightMeasureSpec)
        val min = width.coerceAtMost(height)
        mTranslateX = (width * 0.5f).toInt()
        mTranslateY = (height * 0.5f).toInt()
        val arcDiameter = min - paddingLeft
        mArcRadius = arcDiameter / 2
        val top = (height / 2 - arcDiameter / 2).toFloat()
        val left = (width / 2 - arcDiameter / 2).toFloat()
        mArcRect[left, top, left + arcDiameter] = top + arcDiameter
        updateIndicatorIconPosition()
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDraw(canvas: Canvas) {
        if (!isClockwise) {
            canvas.scale(-1f, 1f, mArcRect.centerX(), mArcRect.centerY())
        }
        // get text and value
        val text = mText
        val value = mPoints.toString() + mUnit

        // center the text
        val xPosValue = width / 2 - mValueRect.width() / 2
        val yPosValue = mArcRect.centerY().toInt() + mTextRect.height()
        val xPosText = width / 2 - mTextRect.width() / 2
        val yPosText = mArcRect.centerY().toInt() - mValueRect.height()

        //draw the text
        if (text != null) {
            mTextPaint.getTextBounds(text, 0, text.length, mTextRect)
            canvas.drawText(text, xPosText.toFloat(), yPosText.toFloat(), mTextPaint)
        }

        // draw the value
        mValuePaint.getTextBounds(value, 0, value.length, mValueRect)
        canvas.drawText(value, xPosValue.toFloat(), yPosValue.toFloat(), mValuePaint)

        // draw the arc and progress
        canvas.drawArc(
            mArcRect, ANGLE_OFFSET.toFloat(), 360f, false,
            mArcPaint
        )
        canvas.drawArc(
            mArcRect, ANGLE_OFFSET.toFloat(), mProgressSweep, false,
            mProgressPaint
        )
        if (mEnabled) {
            // draw the indicator icon
            canvas.translate(
                (mTranslateX - mIndicatorIconX).toFloat(),
                (mTranslateY - mIndicatorIconY).toFloat()
            )
            mIndicatorIcon?.draw(canvas)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (mEnabled) {
            this.parent.requestDisallowInterceptTouchEvent(true)
            when (event.action) {
                MotionEvent.ACTION_DOWN -> if (mOnPointsChangeListener != null) mOnPointsChangeListener?.onStartTrackingTouch(
                    this
                )
                MotionEvent.ACTION_MOVE -> updateOnTouch(event)
                MotionEvent.ACTION_UP -> {
                    if (mOnPointsChangeListener != null) mOnPointsChangeListener?.onStopTrackingTouch(
                        this
                    )
                    isPressed = false
                    this.parent.requestDisallowInterceptTouchEvent(false)
                }
                MotionEvent.ACTION_CANCEL -> {
                    if (mOnPointsChangeListener != null) {
                        mOnPointsChangeListener?.onStopTrackingTouch(this)
                    }
                    isPressed = false
                    this.parent.requestDisallowInterceptTouchEvent(false)
                }
            }
            return true
        }
        return false
    }

    override fun drawableStateChanged() {
        super.drawableStateChanged()
        if (mIndicatorIcon != null && mIndicatorIcon!!.isStateful) {
            val state = drawableState
            mIndicatorIcon?.state = state
        }
        invalidate()
    }

    private fun updateOnTouch(event: MotionEvent) {
        isPressed = true
        mTouchAngle = convertTouchEventPointToAngle(event.x, event.y)
        val progress = convertAngleToProgress(mTouchAngle)
        updateProgress(progress, true)
    }

    private fun convertTouchEventPointToAngle(xPos: Float, yPos: Float): Double {
        // transform touch coordinate into component coordinate
        var x = xPos - mTranslateX
        val y = yPos - mTranslateY
        x = if (isClockwise) x else -x
        var angle = Math.toDegrees(Math.atan2(y.toDouble(), x.toDouble()) + Math.PI / 2)
        angle = if (angle < 0) angle + 360 else angle
        return angle
    }

    private fun convertAngleToProgress(angle: Double): Int {
        return Math.round(valuePerDegree() * angle).toInt()
    }

    private fun valuePerDegree(): Float {
        return mMax.toFloat() / 360.0f
    }

    private fun updateIndicatorIconPosition() {
        val thumbAngle = (mProgressSweep + 90).toInt()
        mIndicatorIconX = (mArcRadius * cos(Math.toRadians(thumbAngle.toDouble()))).toInt()
        mIndicatorIconY = (mArcRadius * sin(Math.toRadians(thumbAngle.toDouble()))).toInt()
    }

    private fun updateProgress(progress: Int, fromUser: Boolean) {

        // detect points change closed to max or min
        var progress = progress
        val maxDetectValue = (mMax.toDouble() * 0.95).toInt()
        val minDetectValue = (mMax.toDouble() * 0.05).toInt() + mMin
        mUpdateTimes++
        if (progress == INVALID_VALUE) {
            return
        }

        // avoid accidentally touch to become max from original point
        if (progress > maxDetectValue && mPreviousProgress == INVALID_VALUE.toFloat()) {
            return
        }
        // record previous and current progress change
        if (mUpdateTimes == 1) {
            mCurrentProgress = progress.toFloat()
        } else {
            mPreviousProgress = mCurrentProgress
            mCurrentProgress = progress.toFloat()
        }
        if (mUpdateTimes > 1 && !isMin && !isMax) {
            mPoints = progress
            if (mPreviousProgress >= maxDetectValue && mCurrentProgress <= minDetectValue && mPreviousProgress > mCurrentProgress) {
                isMax = true
                progress = mMax
                mPoints = mMax
                if (mOnPointsChangeListener != null) {
                    mOnPointsChangeListener!!
                        .onPointsChanged(this, progress, fromUser)
                    return
                }
            } else if (mCurrentProgress >= maxDetectValue && mPreviousProgress <= minDetectValue && mCurrentProgress > mPreviousProgress || mCurrentProgress <= mMin) {
                isMin = true
                progress = mMin
                mPoints = mMin
                if (mOnPointsChangeListener != null) {
                    mOnPointsChangeListener!!
                        .onPointsChanged(this, progress, fromUser)
                    return
                }
            }
            invalidate()
        } else {

            // Detect whether decreasing from max or increasing from min, to unlock the update event.
            // Make sure to check in detect range only.
            if (isMax and (mCurrentProgress < mPreviousProgress) && mCurrentProgress >= maxDetectValue) {
                isMax = false
            }
            if (isMin
                && mPreviousProgress < mCurrentProgress
                && mPreviousProgress <= minDetectValue && mCurrentProgress <= minDetectValue && mPoints >= mMin
            ) {
                isMin = false
            }
        }
        if (!isMax && !isMin) {
            progress = if (progress > mMax) mMax else progress
            progress = if (progress < mMin) mMin else progress
            if (mOnPointsChangeListener != null) {
                progress -= progress % step
                mOnPointsChangeListener?.onPointsChanged(this, progress, fromUser)
            }
            mProgressSweep = progress.toFloat() / valuePerDegree()
            updateIndicatorIconPosition()
            invalidate()
        }
    }

    interface OnPointsChangeListener {
        fun onPointsChanged(circularProgressBar: CircularProgressBar?, points: Int, fromUser: Boolean)
        fun onStartTrackingTouch(circularProgressBar: CircularProgressBar?)
        fun onStopTrackingTouch(circularProgressBar: CircularProgressBar?)
    }

    var value: Int
        get() = mPoints
        set(points) {
            var points = points
            points = if (points > mMax) mMax else points
            points = if (points < mMin) mMin else points
            updateProgress(points, false)
        }
    var progressWidth: Int
        get() = mProgressWidth
        set(mProgressWidth) {
            this.mProgressWidth = mProgressWidth
            mProgressPaint.strokeWidth = mProgressWidth.toFloat()
        }
    var arcWidth: Int
        get() = mArcWidth
        set(mArcWidth) {
            this.mArcWidth = mArcWidth
            mArcPaint.strokeWidth = mArcWidth.toFloat()
        }

    override fun isEnabled(): Boolean {
        return mEnabled
    }

    override fun setEnabled(enabled: Boolean) {
        mEnabled = enabled
    }

    var progressColor: Int
        get() = mProgressPaint.color
        set(color) {
            mProgressPaint.color = color
            invalidate()
        }
    var arcColor: Int
        get() = mArcPaint.color
        set(color) {
            mArcPaint.color = color
            invalidate()
        }

    fun setTextColor(textColor: Int) {
        mTextPaint.color = textColor
        invalidate()
    }

    fun setTextSize(textSize: Float) {
        mTextSize = textSize
        mTextPaint.textSize = mTextSize
        invalidate()
    }

    fun getMax(): Int {
        return mMax
    }

    fun setMax(mMax: Int) {
        require(mMax > mMin) { "Max should not be less than min." }
        this.mMax = mMax
    }

    fun getMin(): Int {
        return mMin
    }

    fun setMin(min: Int) {
        require(mMax > mMin) { "Min should not be greater than max." }
        mMin = min
    }

    companion object {
        var INVALID_VALUE = -1
        const val MAX = 100
        const val MIN = 0
        private const val ANGLE_OFFSET = -90
    }
}