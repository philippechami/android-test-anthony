package com.example.euriskotraining.utilities


class GlobalVariables {
    companion object{
        var secondsFromBegining : Long = 0
        var databaseCreated : Boolean = false
        const val VIEW_TYPE_MESSAGE_RIGHT: Int = 1
        const val VIEW_TYPE_MESSAGE_LEFT: Int = 2
        const val MESSAGE_TYPE_TEXT: Int = 1
        const val MESSAGE_TYPE_PHOTO: Int = 2
        const val MESSAGE_TYPE_AUDIO: Int = 3
        const val MESSAGE_TYPE_DOCUMENT: Int = 4
        const val DATE_HH_MM_A: String = "hh:mm a"
        const val DATE_DD_M_YYYY: String = "dd/M/yyyy"
        const val DATE_DD_M_YYYY_HH_MM_A: String = "dd/M/yyyy hh:mm a"
        const val YESTERDAY: String = "Yesterday"
        const val TODAY: String = "Today"
        const val IMAGE_WIDTH_DP: Int = 200
        const val IMAGE_HEIGHT_DP: Int = 200
        const val ENGLISH: String = "en"
        const val ARABIC: String = "ar"
        const val LANG_KEY: String = "langCode"
        const val PREF_NAME: String = "settings"

    }

}