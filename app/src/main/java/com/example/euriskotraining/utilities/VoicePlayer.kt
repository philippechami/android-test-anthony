package com.example.euriskotraining.utilities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.TextView
import java.io.IOException
import java.lang.Exception


class VoicePlayer(private val context: Context) {
    private lateinit var imgPlay: ImageView
    private lateinit var imgPause: ImageView
    private lateinit var seekBar: SeekBar
    private lateinit var txtProcess: TextView
    private lateinit var mediaPlayer: MediaPlayer

    @SuppressLint("SetTextI18n")
    fun init(path: String, imgPlay: ImageView, imgPause: ImageView, seekBar: SeekBar, txtProcess: TextView) {
        this.imgPlay = imgPlay
        this.imgPause = imgPause
        this.seekBar = seekBar
        this.txtProcess = txtProcess
        mediaPlayer = MediaPlayer()

        try {
            mediaPlayer.setDataSource(path)
            mediaPlayer.prepare()
            mediaPlayer.setVolume(10f, 10f)
            //START and PAUSE are in other listeners
            mediaPlayer.setOnPreparedListener { mp ->
                seekBar.max = mp.duration
                txtProcess.text = "0:${convertTime((mp.duration / 1000).toLong())}"
            }
            mediaPlayer.setOnCompletionListener {
                imgPause.visibility = View.GONE
                imgPlay.visibility = View.VISIBLE
            }
        } catch (e: IOException) {
            LocalFunctions.printException(e)
        }

        this.seekBar.setOnSeekBarChangeListener(seekBarListener)
        this.imgPlay.setOnClickListener(imgPlayClickListener)
        this.imgPause.setOnClickListener(imgPauseClickListener)
    }

    //Components' listeners
    private var imgPlayClickListener = View.OnClickListener {

        imgPause.visibility = View.VISIBLE
        imgPlay.visibility = View.GONE
        mediaPlayer.start()
        try {
            update(mediaPlayer, txtProcess, seekBar, context)
        } catch (e: Exception) {
            LocalFunctions.printException(e)
        }
    }
    private val seekBarListener: OnSeekBarChangeListener = object : OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            if (fromUser) {
                mediaPlayer.seekTo(progress)
            }
        }

        override fun onStartTrackingTouch(seekBar: SeekBar) {
            imgPause.visibility = View.GONE
            imgPlay.visibility = View.VISIBLE
        }

        override fun onStopTrackingTouch(seekBar: SeekBar) {
            imgPlay.visibility = View.GONE
            imgPause.visibility = View.VISIBLE
            mediaPlayer.start()
        }
    }
    private var imgPauseClickListener = View.OnClickListener {
        imgPause.visibility = View.GONE
        imgPlay.visibility = View.VISIBLE
        mediaPlayer.pause()
    }

    //Updating seekBar in realtime
    private fun update(
        mediaPlayer: MediaPlayer,
        time: TextView,
        seekBar: SeekBar,
        context: Context
    ) {
        (context as Activity).runOnUiThread {
            seekBar.progress = mediaPlayer.currentPosition
            if (mediaPlayer.duration - mediaPlayer.currentPosition > 100) {
                "${convertTime((mediaPlayer.currentPosition / 1000).toLong())} / ${convertTime((mediaPlayer.duration / 1000).toLong())}".also { time.text = it }

            } else {
                time.text =
                    convertTime((mediaPlayer.duration / 1000).toLong())
                seekBar.progress = 0
            }
            val handler = Handler(Looper.getMainLooper())
            try {
                val runnable = Runnable {
                    try {
                        if (mediaPlayer.currentPosition > -1) {
                            try {
                                update(mediaPlayer, time, seekBar, context)
                            } catch (e: Exception) {
                                LocalFunctions.printException(e)
                            }
                        }
                    } catch (e: Exception) {
                        LocalFunctions.printException(e)
                    }
                }
                handler.postDelayed(runnable, 2)
            } catch (e: Exception) {
                LocalFunctions.printException(e)
            }
        }
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        var mInstance: VoicePlayer? = null

        fun getInstance(context: Context): VoicePlayer? {
            mInstance = VoicePlayer(context)
            return mInstance
        }

        //Convert long milli seconds to a formatted String to display it
        private fun convertTime(seconds: Long): String {
            val s = seconds % 60
            val m = seconds / 60 % 60
            val h = seconds / (60 * 60) % 24
            if(h == 0L && m == 0L){
                return String.format("%02d", s)
            }
            if(h == 0L){
                return String.format("%02d:%02d", m, s)
            }
            return String.format("%02d:%02d:%02d", h, m, s)
        }
    }
}