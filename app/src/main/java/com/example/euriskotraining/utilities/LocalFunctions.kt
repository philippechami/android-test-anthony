package com.example.euriskotraining.utilities

import com.example.euriskotraining.BuildConfig


class LocalFunctions {
    companion object {
        fun printException(e: Exception) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace()
            }
        }
    }
}