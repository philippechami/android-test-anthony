package com.example.euriskotraining.utilities

import com.example.euriskotraining.utilities.GlobalVariables.Companion.DATE_DD_M_YYYY
import com.example.euriskotraining.utilities.GlobalVariables.Companion.DATE_DD_M_YYYY_HH_MM_A
import com.example.euriskotraining.utilities.GlobalVariables.Companion.DATE_HH_MM_A
import java.text.SimpleDateFormat
import java.util.*

class DateUtils() {

    companion object{

        private val dateFormat = SimpleDateFormat(DATE_DD_M_YYYY_HH_MM_A)
        private val dayFormat = SimpleDateFormat(DATE_DD_M_YYYY)
        private val timeFormat = SimpleDateFormat(DATE_HH_MM_A)

        fun getDayFormattedFromStr(dateStr: String) : String{
            val date = dateFormat.parse(dateStr)
            return dayFormat.format(date)
        }
        fun getTimeFormattedFromStr(dateStr: String) : String {
            val date = dateFormat.parse(dateStr)
            return timeFormat.format(date)
        }
        fun getDayFromStr(dateStr: String) : Date{
            return dayFormat.parse(dateStr)
        }

        fun getDateFormatted(date: Date): String{
            return dateFormat.format(date)
        }

        fun getYesterday(): Date{
            val cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);
            val dateFormatted = dayFormat.format(cal.time);
            return dayFormat.parse(dateFormatted)
        }
        fun getToday(): Date{
            return dayFormat.parse(dateFormat.format(Date()))
        }
    }

}