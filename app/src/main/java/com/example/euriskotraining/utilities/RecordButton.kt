package com.example.euriskotraining.utilities

import android.content.Context
import android.view.MotionEvent
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.widget.AppCompatImageButton
import com.example.euriskotraining.interfaces.OnRecordButtonClicked

class RecordButton(context: Context) : AppCompatImageButton(context), View.OnTouchListener {
    var isListenForRecording = false
    private lateinit var recordView: RecordView
    private lateinit var recordBtn: ImageButton
    private lateinit var listener: OnRecordButtonClicked


    override fun onTouch(v: View, event: MotionEvent?): Boolean {
        if(isListenForRecording) {
            when(event?.action) {
                MotionEvent.ACTION_DOWN ->
                    recordView.onActionDown()
                MotionEvent.ACTION_UP ->
                    recordView.onActionUp()
            }
        } else {
            listener.onClick()
        }
        return isListenForRecording
    }
    fun init(recordView: RecordView, recordButton: ImageButton, listener: OnRecordButtonClicked){
        this.recordView = recordView
        this.recordBtn = recordButton
        this.listener = listener
        recordBtn.setOnTouchListener(this)
    }
}