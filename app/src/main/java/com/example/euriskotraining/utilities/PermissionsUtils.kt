package com.example.euriskotraining.utilities

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import com.example.euriskotraining.BaseActivity

class PermissionsUtils(){
    companion object{
        fun isReadStoragePermissionGranted(context: Context): Boolean{
            var readExternalStorage = 0
            readExternalStorage = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)
            return readExternalStorage == PackageManager.PERMISSION_GRANTED
        }
        fun isWriteStoragePermissionGranted(context: Context): Boolean {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                return true
            }
            var readExternalStorage = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            return readExternalStorage == PackageManager.PERMISSION_GRANTED
        }
        fun isRecordPermissionGranted(context: Context): Boolean{
            var recordPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO)
            return recordPermission == PackageManager.PERMISSION_GRANTED
        }
        fun isCamPermissionGranted(context: Context): Boolean {
            val camPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)

            return camPermission == PackageManager.PERMISSION_GRANTED
        }
    }


}