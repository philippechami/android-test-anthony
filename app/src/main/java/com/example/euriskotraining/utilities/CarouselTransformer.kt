package com.example.euriskotraining.utilities

import android.view.View
import androidx.viewpager2.widget.ViewPager2

class CarouselTransformer: ViewPager2.PageTransformer {

    override fun transformPage(page: View, position: Float) {
        page.apply {
            when {
                position >= -1f && position < 0f -> {//between left and middle
                    this.rotation = 25f * position
                    this.translationY = -165f * position
                }
                position > 0f && position <= 1f -> {//between right and middle
                    this.rotation = 25f * position
                    this.translationY = 165f * position
                }
            }
        }
    }

}