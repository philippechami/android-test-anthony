package com.example.euriskotraining.utilities

import android.content.Context
import android.os.SystemClock
import android.view.View
import android.widget.Chronometer
import android.widget.ImageView
import android.widget.RelativeLayout
import com.example.euriskotraining.interfaces.OnRecordListener


class RecordView(context: Context) : RelativeLayout(context) {

    private lateinit var parentLayout: RelativeLayout
    private lateinit var micImageView: ImageView
    private lateinit var chronometer: Chronometer
    private lateinit var onRecordListener: OnRecordListener

    fun init(parentLayout: RelativeLayout, micIV: ImageView, chronometer: Chronometer) {
        this.parentLayout = parentLayout
        this.micImageView = micIV
        this.chronometer = chronometer
    }

    fun setOnRecordListener(recordListener: OnRecordListener){
        this.onRecordListener = recordListener
    }

    fun onActionDown() {
        startRecording()
    }
    fun onActionUp() {
        stopRecording()
    }
    private fun hideView(){
        parentLayout.visibility = View.GONE
    }
    private fun showView() {
        parentLayout.visibility = View.VISIBLE
    }
    private fun startRecording() {
        if(onRecordListener != null) {
            onRecordListener.onStart()
        }
        chronometer.base = SystemClock.elapsedRealtime()
        chronometer.start()
        showView()
    }
    private fun stopRecording() {
        if(onRecordListener != null) {
            onRecordListener.onFinish()
        }
        chronometer.stop()
        hideView()
    }

}