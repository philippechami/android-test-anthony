package com.example.euriskotraining.di

import android.content.Context
import androidx.room.Room
import com.example.euriskotraining.data.local.AppDatabase
import com.example.euriskotraining.data.local.ChatDao
import com.example.euriskotraining.data.local.TimerDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Provides
    fun provideTimeDao(appDatabase: AppDatabase) : TimerDao {
        return appDatabase.timerDao()
    }
    @Provides
    fun provideChatDao(appDatabase: AppDatabase) : ChatDao {
        return appDatabase.chatDao()
    }

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext appContext : Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            AppDatabase.DB_NAME
        ).build()
    }

}