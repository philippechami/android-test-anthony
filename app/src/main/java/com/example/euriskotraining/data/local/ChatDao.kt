package com.example.euriskotraining.data.local

import androidx.room.*
import com.example.euriskotraining.data.entity.ChatEntity

@Dao
interface ChatDao {
    @Query("SELECT * FROM chats ORDER BY cid ASC")
    suspend fun getMessages() : List<ChatEntity>?

    @Insert
    suspend fun insert(chatEntity: ChatEntity)

}