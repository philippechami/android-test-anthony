package com.example.euriskotraining.data.repositories

import com.example.euriskotraining.data.entity.ChatEntity
import com.example.euriskotraining.data.local.ChatDao
import com.example.euriskotraining.data.entity.TimerModel
import com.example.euriskotraining.data.local.TimerDao
import javax.inject.Inject

class DatabaseRepo @Inject constructor(private val timerDao: TimerDao, private val chatDao: ChatDao) {

    suspend fun getTimer(id: Long?) : TimerModel? = timerDao.getTimer(id)
    suspend fun insert(timerModel: TimerModel): Long = timerDao.insert(timerModel)
    suspend fun update(timerModel: TimerModel) = timerDao.update(timerModel)

    suspend fun getMessages() : List<ChatEntity>? = chatDao.getMessages()
    suspend fun insert(chatEntity: ChatEntity) = chatDao.insert(chatEntity)
}