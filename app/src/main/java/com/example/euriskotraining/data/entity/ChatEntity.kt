package com.example.euriskotraining.data.entity


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = ChatEntity.TABLE_NAME
)
data class ChatEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "cid") var id: Long = 0,
    @ColumnInfo(name = TEXT) var text: String = "",
    @ColumnInfo(name = FILE) var file: String = "",
    @ColumnInfo(name = MESSAGE_TYPE) var messageType: Int = 0,
    @ColumnInfo(name = USER_TYPE) var userType: Int = 0,
    @ColumnInfo(name = DATE) var date: String = ""
) {
    constructor() : this(0L, "","", 0, 0,"")
    companion object{
        const val TABLE_NAME = "chats"
        const val TEXT = "text"
        const val DATE = "date"
        const val USER_TYPE = "userType"
        const val MESSAGE_TYPE = "messageType"
        const val FILE = "file"
    }
}