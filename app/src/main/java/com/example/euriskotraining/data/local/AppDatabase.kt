package com.example.euriskotraining.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.euriskotraining.data.entity.ChatEntity
import com.example.euriskotraining.data.entity.TimerModel

@Database(entities = [TimerModel::class, ChatEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun timerDao(): TimerDao
    abstract fun chatDao() : ChatDao
    companion object {
        const val DB_NAME = "app.db"
    }
}