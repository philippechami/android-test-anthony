package com.example.euriskotraining.data.local

import androidx.room.*
import com.example.euriskotraining.data.entity.TimerModel

@Dao
interface TimerDao {
    @Query("SELECT * FROM timers WHERE uid = :id LIMIT 1")
    suspend fun getTimer(id: Long?) : TimerModel?

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(timerModel: TimerModel): Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun update(timerModel: TimerModel)
}