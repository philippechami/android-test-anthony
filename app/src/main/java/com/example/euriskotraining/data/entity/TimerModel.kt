package com.example.euriskotraining.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = TimerModel.TABLE_NAME,
    indices = [Index(value = [TimerModel.USING_APP_TIME], unique = true)])
data class TimerModel(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "uid") var id: Long = 0,
    @ColumnInfo(name = USING_APP_TIME) var timer: Long = 0,
) {


    companion object {
        const val TABLE_NAME = "timers"
        const val USING_APP_TIME = "using_app_time"

    }
}