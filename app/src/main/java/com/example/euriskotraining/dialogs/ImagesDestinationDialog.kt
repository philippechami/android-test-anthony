package com.example.euriskotraining.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.euriskotraining.databinding.FragmentDialogImageDestinationBinding
import com.example.euriskotraining.interfaces.OnAttachmentsClicked

class ImagesDestinationDialog(val listener: OnAttachmentsClicked): DialogFragment() {

    private lateinit var binding: FragmentDialogImageDestinationBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentDialogImageDestinationBinding.inflate(inflater, container, false)

        binding.tvCancel.setOnClickListener{
            dismiss()
        }
        binding.rlCamera.setOnClickListener{
            listener.onCameraClicked()
        }
        binding.rlGallery.setOnClickListener{
            listener.onGalleryClicked()
        }
        binding.rlDocuments.setOnClickListener {
            listener.onDocumentsClicked()
        }

        return binding.root
    }
}