package com.example.euriskotraining.interfaces

interface OnRecordListener {
    fun onStart()
    fun onCancel()
    fun onFinish()
    fun onLessThanSecond()
}