package com.example.euriskotraining.interfaces

interface OnRecordButtonClicked {
    fun onClick()
}