package com.example.euriskotraining.interfaces

interface OnAttachmentsClicked {
    fun onGalleryClicked()
    fun onCameraClicked()
    fun onDocumentsClicked()
}